<?php
	class Cliente extends CI_Controller{

		function __construct(){

			parent::__construct();
			$this->load->model('Cliente_model', 'cliente');
		}


		public function listar(){

			$lista = $this->cliente->listar();

			if($lista != false){

				$res["res"] = "ok"; 
				$res["lista"] = $lista;

			}else{
				$res["res"] = "failed";
			}

			echo json_encode($res);
		}

		public function agregar(){

			$param["cln_descripcion"] = $this->input->post("nombre");

			$agregar = $this->cliente->agregar($param);

			if($agregar != false){

				$res["res"] = "ok"; 

			}else{
				$res["res"] = "failed";
			}

			echo json_encode($res);
		}

		public function actualizar(){

			$id = intval($this->input->post('id'));

            $param['cln_descripcion'] = $this->input->post('nombre');
          
            $editar = $this->cliente->editar($param, $id);

            if($editar != false){
              $res["res"] = "ok";
  		      echo json_encode($res);
            }
		}


		public function eliminar($id){
            
           /*if(!$this->session->has_userdata('admin')){
              exit();
            }*/
            
            $this->cliente->eliminar($id);


            $res["res"] = "ok";
            echo json_encode($res);
        }
	}
?>