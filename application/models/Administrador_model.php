<?php
	class Administrador_model extends CI_Model{

		function __construct(){

			parent::__construct();
			$this->load->library('session');
		}

		public function login($usuario, $clave){

			if(isset($usuario) && isset($clave)){

              $this->db->select('*');
              $this->db->from('usuario');
              $this->db->where('usr_email', $usuario);
              $this->db->where('usr_pass', $clave);

              // Obtenemos el resultado de la consulta
              $resultado = $this->db->get();
              if($resultado->num_rows() == 1) {
                $r = $resultado->row(); // guarda el registro de la consulta        

                $s_usuario =  array('idUsuario' => $r->usr_id,
                					'nombreUsuario' => $r->usr_nombre,
                               	   	'Conexion' => TRUE);

                //Ponemos en session
                $this->session->set_userdata($s_usuario);

                $result = array('respuesta' => 1);

              }else{
                  $result = array('respuesta' => 'Usuario y/o contraseña errónea.');
              }
          	}else{
              	$result = array('respuesta' => 'NO HUBO ENVÍO DE PARÁMETROS');

          	}

          	return $result;
		}

	} 
?>