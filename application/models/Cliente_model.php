<?php

	class Cliente_model extends CI_Model{

		function __construct(){

			parent::__construct();
		}

        public function listar(){
          
            $sql = 'SELECT * from cliente ORDER BY cln_id asc';

            $query = $this->db->query($sql);

            if($query->num_rows()>0){
                return $query->result();
            }else{
                return FALSE;
            }
        }

        public function agregar($param){
         
          return $this->db->insert('cliente', $param);

        }

        public function editar($param, $id){
    
            $this->db->where('cln_id', $id);
            $result = $this->db->update('cliente', $param);

            return $result;
        }


        public function eliminar($id){
            $this->db->where('cln_id', $id);
            $result = $this->db->delete('cliente');

            return $result;
        }



	}
?>