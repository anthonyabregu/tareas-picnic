<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="UTF-8">
	<title>Menorca - Leads</title>

	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/libs/bootstrap/css/bootstrap.min.css">
	<!--<link rel="stylesheet" href="bootstrap/css/bootstrap-flaty.min.css">-->

	<script src="<?php echo base_url(); ?>assets/libs/jquery-3.3.1.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/admin/js/popper.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/admin/js/jquery-ui.js"></script>
	<script src="<?php echo base_url(); ?>assets/libs/bootstrap/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/libs/jquery.form.js"></script>

	<script>
	var path = '<?php echo base_url(); ?>';
	</script>
	<style>
	form{
		width: 400px;
		margin: auto;
	}
	input{
		margin: 20px 0;
	}
	.btn-success{
		margin: auto;
		display: block;
		width: 100%;
	}
	.franja{
		background-color: #000;
		padding-top: 40px;
		padding-bottom: 20px;
	}
	.franja .logo{
		display: block;
		margin: auto;
	}
	</style>
</head>
<body>
<div class="franja">
		<!-- <img src="<?php echo base_url() ?>assets/web/img/logotipo.svg" width="80" alt="" class="logo"> -->
	</div>
<div id="login" class="container">


	<form class="form-signin" role="form" action="<?php echo base_url() ?>administrador/login" method="post">
		<br>

		<input type="email" class="form-control inuser" placeholder="Email" name="usuario" required autofocus>
		<input type="password" class="form-control inclave" placeholder="Clave" name="clave" required>

		<button class="btn btn-success" type="submit">Entrar</button>
		<br><br>
		<?php if(isset($_GET["error"])){ ?>
		<div class="alert alert-danger">¡Usuario y/o clave incorrectos!</div>
		<?php } ?>
	</form>
</div>

</body>
</html>
