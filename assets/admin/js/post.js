
var textarea = `<label>Contenido</label>
                <div class="edit contenido"></div>
                <textarea name="contenido" style="display:none;"></textarea>`;


$(document).ready(function () {

    // Initialize the editor.
     /*$('textarea#froala-editor').froalaEditor();*/

    listar(1); // Listamos los post en la tabla

    $("#post select[name=estado]").bind("change",function(){
		var es = $(this).val();
		listar(es);
	});

    $("#post select[name=categoria_post]").bind("change",function(){
		var ctg = $(this).val();
		filtro_categoria(ctg);
	});


    var imagen = $('#modalagregar .cuerpo-modal').html();

    $("#modalagregar select[name=tipo]").bind("change",function(){

		//console.log(imagen);
		if($(this).val() == 1 ){

            $('#contenido-textarea').css("display", "block");
		}
		else{
            $('#contenido-textarea').css("display", "none");
		}
	});

    var estado = 1;

    new Request("categoriapost/listar/", {
        estado: estado
    }, function (res) {
        $.each(res,function(k,v){
            var option = '<option value="'+v.ctg_id+'">'+v.ctg_descripcion+'</option>'
            $(".filtros select[name=categoria_post]").append(option);
            $("#modalagregar select[name=categoria_post]").append(option);
        });
    });

    $("#banners .lista" ).sortable({
		items: "> tr",
		opacity: 0.7,
		update:function(event,ui){

			$(".orden").show();
		}
	});

    /* envia form del modalagregar*/
    var options = {
		dataType: 'json',
		type: 'post',
		clearForm:true,
        beforeSubmit:function(){
    		$("#cargando").fadeIn(200);
            $("#modalagregar .send").attr("disabled", true); //desactivar el boton mientras se sube la informacion
    	},
        beforeSerialize: function ($form, options) {
            // return false to cancel submit
            $("#modalagregar textarea[name=contenido]").val($('#modalagregar .contenido .ql-editor').html());
        },
		success: function(res){
			console.log(res.res=="ok");
			if(res.res=="ok"){
                //$("#modalagregar textarea[name=contenido]").val($('#modalagregar .contenido .ql-editor').html());
				$("#modalagregar").modal("hide");
				listar(res.estado);
			}
		}
	}
    $("#modalagregar form").ajaxForm(options);

      /* envia form del modaleditar*/
      var options = {
  		dataType: 'json',
  		type: 'post',
  		clearForm:true,
        beforeSubmit:function(){
    		$("#cargando").fadeIn(200);
            $("#modaleditar .send").attr("disabled", true);
    	},
        beforeSerialize: function ($form, options) {
            // return false to cancel submit
            $("#modaleditar textarea[name=contenido]").val($('#modaleditar .contenido .ql-editor').html());
        },
  		success: function(res){
  			console.log(res.res=="ok");
  			if(res.res=="ok"){
  				console.log(res.res=="ok");
                $("#cargando").fadeOut(200);
  				$("#modaleditar").modal("hide");
                $("#modaleditar .send").attr("disabled", false);
  				listar(res.estado);
  			}
  		}
  	}
      $("#modaleditar form").ajaxForm(options);


    $('.edit').each(function(index){
      var container = $('.edit').get(index);
          var options = {
              modules: {
                 toolbar:  [
                   ['bold', 'italic', 'underline'],
                   [{'list':'ordered'},{'list':'bullet'}],
                    [{ 'align': [] }],
                    ['link', 'image']
                ],
                table: true
               },
               theme: 'snow'  // or 'bubble'
          }
          new Quill(container,options);
      });


});

function filtro_categoria(ctg){
    $("#post .lista").empty();
	listahtml = Array();
	new Request("post/filtro/"+ctg,null,function(res){
		lista = res;
		llenar();
	});

}


function listar(es){
	$("#post .lista").empty();
	listahtml = Array();
	new Request("post/listar/"+es,null,function(res){
		/*lista = res;
		llenar();*/
        if(res.res != 'empty'){
            lista = res;
    		llenar();
        }
        else{
          alert('No hay post publicados en esta categoria y estado.');
      }
	});
}

function llenar(){
	$.each(lista,function(k,v){
		var it = new ItemPost(v);
		$("#post .lista").append(it);
	})
}


var ItemPost = function(data){
    var img = '';
	var descripcion_tipo = '';
	var tipo = (data.pst_tipo);
	//console.log(tipo);
	if(tipo == 1)
	{
		img = path+'files/posts/'+data.pst_imagen;
		descripcion_tipo = 'Imagen';
	}
	else{
		img = path+'files/posts/'+data.pst_imagen;
		descripcion_tipo = 'Video';
	}

	var html = $('<tr width="100%" data-id="'+data.pst_id+'">'+
                    '<td><img src="' + img + '" width="50"></td>' +
					'<td>'+data.pst_titulo+'</td>'+
					'<td>'+data.pst_contenido+'</td>'+
                    '<td>'+data.pst_link+'</td>'+
                    '<td>'+descripcion_tipo+'</td>'+
					'<td><div class="btn-group" role="group" aria-label="...">'+
  			'<button type="button" class="btn btn-outline-primary btn-editar"><i class="fas fa-edit"></i></button>'+
			'</div></td>'+
				'</tr>');


	html.find(".btn-editar").click(function(){
		console.log(data);
        if(tipo == 1){
            $('.contenido-textarea').css("display", "block");
            $("#modaleditar input[name=id]").val(data.pst_id);
            $("#modaleditar .preview").attr("src","");
            $("#modaleditar .preview").attr("src",path+"files/posts/"+data.pst_imagen);
    		$("#modaleditar input[name=titulo]").val(data.pst_titulo);
    		//$("#modaleditar .foto").attr("src","");
    		//$("#modaleditar .foto").attr("src",path+"files/posts/"+data.bnn_foto);
            $('#modaleditar .contenido .ql-editor').html(data.pst_contenido);
    		$("#modaleditar textarea[name=contenido]").val(data.pst_contenido);
    		$("#modaleditar input[name=link]").val(data.pst_link);
    		//$("#modaleditar input[name=boton]").val(data.bnn_boton);
    		$("#modaleditar select[name=estado]").val(data.pst_estado);
    		$("#modaleditar select[name=categoria_post]").val(data.ctg_id);
            //$("#modaleditar .preview").css("background-image","url("+path+"files/oists/"+data.pst_imagen+")");
    		$("#modaleditar .msg").attr("src","");
    		//$("#modaleditar .msg").attr("src",path+"files/posts/"+data.bnn_foto);

    		$("#modaleditar .preview").removeClass("pL");
    		$("#modaleditar .preview").removeClass("pC");
    		$("#modaleditar .preview").removeClass("pR");

        }else{
            $('.contenido-textarea').css("display", "none");
            $("#modaleditar input[name=id]").val(data.pst_id);
            $("#modaleditar .preview").attr("src","");
            $("#modaleditar .preview").attr("src",path+"files/posts/"+data.pst_imagen);
    		$("#modaleditar input[name=titulo]").val(data.pst_titulo);
    		//$("#modaleditar .foto").attr("src","");
    		//$("#modaleditar .foto").attr("src",path+"files/posts/"+data.bnn_foto);
            //$('#modaleditar .contenido .ql-editor').html(data.pst_contenido);
    		//$("#modaleditar textarea[name=contenido]").val(data.pst_contenido);
    		$("#modaleditar input[name=link]").val(data.pst_link);
    		//$("#modaleditar input[name=boton]").val(data.bnn_boton);
    		$("#modaleditar select[name=estado]").val(data.pst_estado);
    		$("#modaleditar select[name=categoria_post]").val(data.ctg_id);
            //$("#modaleditar .preview").css("background-image","url("+path+"files/oists/"+data.pst_imagen+")");
    		$("#modaleditar .msg").attr("src","");
    		//$("#modaleditar .msg").attr("src",path+"files/posts/"+data.bnn_foto);

    		$("#modaleditar .preview").removeClass("pL");
    		$("#modaleditar .preview").removeClass("pC");
    		$("#modaleditar .preview").removeClass("pR");
        }

		$("#modaleditar").modal("show");



		$("#modaleditar a.eliminar").unbind();
		$("#modaleditar a.eliminar").click(function(){
			$("#modaleditar").modal("hide");
			$("#modaleliminar .btn-eliminar").unbind();
			$("#modaleliminar .btn-eliminar").click(function(){

				new Request("post/eliminar/"+data.pst_id,null,function(res){
					listar(data.pst_estado);
					$("#modaleliminar").modal("hide");
				});
			});
		})
	});
	return html;  // Retormnamos los valores para la tabla
}
