﻿$(document).ready(function () {

    listar();

    //var cookie = $.cookie('usuarioA','anthony', { path: '/', expires: 7 }); // con fecha de expiración a los 7 dias y path

    //var CookieSet = $.cookie('usuario', 'anthony');

    //$("#categorias .filtros select[name=estado]").bind("change",listar);

    var options = {
        dataType: 'json',
        type: 'post',
        clearForm: true,
        success: function (res) {
            if (res.res == "ok") {
                $("#modaleditar").modal("hide");
                listar();
            }
        }
    }
    $("#modaleditar form").ajaxForm(options);

});

function pdf(obj){

  var pdf = $(obj).attr("data-pdf");
  var url = baseurl + "files/pdfproyectos/" + pdf;
  window.open(url, '_blank');

}
function listar() {
    $("#contacto .lista").empty();

    new Request("contactodescarga/listar/", {

    }, function (res) {
        $("#contacto .lista").empty();
        $.each(res, function (k, v) {
            var it = new ItemContacto(v);
            $("#contacto .lista").append(it);
        })

    });


}


var ItemContacto = function (data) {

    var id = data.ctd_id;
    var nombre = data.ctd_nombre;
    var email = data.ctd_email;

    var html = $('<tr width="100%">' +
        '<td style="vertical-align:middle;">' + id + '</td>' +
        '<td style="vertical-align:middle;">' + nombre + '</td>' +
        '<td style="vertical-align:middle;">' + email + '</td>' +
        '<td><div class="btn-group" role="group" aria-label="...">' +
        '<button type="button" class="btn b tn-outline-primary btn-editar" ><i class="fa fa-eye"></i> Detalle</button>' +
        '</div></td>' +
        '</tr>');


    html.find(".btn-editar").click(function () {


        $("#modaleditar input[name=id]").val(id);
        $("#modaleditar input[name=nombre]").val(nombre);
        $("#modaleditar input[name=email]").val(email);

        $("#modaleditar").modal("show");

        $("#modaleditar a.eliminar").unbind();
        $("#modaleditar a.eliminar").click(function () {
            $("#modaleditar").modal("hide");
            $("#modaleliminar .btn-eliminar").unbind();
            $("#modaleliminar .btn-eliminar").click(function () {

                new Request("contactodescarga/eliminar/" + data.cto_id, {
                }, function (res) {
                    listar();
                    $("#modaleliminar").modal("hide");
                });
            });

        })


    });

    return html;

};
