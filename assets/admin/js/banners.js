var lista;
var tipo_video = `<div class="form-group">
					<label>Cambiar imagen de fondo (JPG 447)</label>
					<input type="file" name="fondo" accept=".jpg,.jpeg"  class="form-control-file">
				  </div>
				  <div class="form-group">
					<label>Url de Video</label>
					<input type="text" name="url" class="form-control">
				  </div>
				  <div class="form-group">
				  	<label>Nombre (Referencia)</label>
					<input type="text" name="nombre" class="form-control">
				  </div>

				  <div class="form-group">
					<label>Estado</label>
					<select name="estado" class="form-control" required>
						<option value="1" selected>Habilitado</option>
						<option value="0">Deshabilitado</option>
					</select>
				  </div>`;

 var tipo_imagen = `<div class="img-thumbnail preview">
   						<img src="" alt="" class="msg">
 					</div>
 					<br>
 					<div class="form-group">
   						<label>Cambiar imagen de fondo (JPG 447)</label>
   						<input type="file" name="fondo" accept=".jpg,.jpeg"  class="form-control-file">
 					</div>
 					<div class="form-group">
   						<label>Posición del mensaje (Horizontal)</label>
   						<select name="posicion" class="form-control" required>
	 						<option value="L">Izquierda</option>
	 						<option value="C">Centro</option>
	 						<option value="R">Derecha</option>
   						</select>
 					</div>
 					<div class="form-group">
   						<label>Pre-Título</label>
   						<input type="text" name="pre-titulo" class="form-control">
 					</div>
 					<div class="form-group">
   						<label>Título</label>
   						<input type="text" name="titulo" class="form-control">
 					</div>
 					<div class="form-group">
   						<label>Texto Botón</label>
   						<input type="text" name="boton" class="form-control">
 					</div>
 					<div class="form-group">
   						<label>Estado</label>
   						<select name="estado" class="form-control" required>
	 						<option value="1">Habilitado</option>
	 						<option value="0">Deshabilitado</option>
   						</select>
 					</div>`;

 var caja_imagen = `<div class="img-thumbnail preview">
						<img src="" alt="" class="msg">
				    </div>`;



$(document).ready(function(){

	listar(1);

	$("#banners select[name=estado]").bind("change",function(){
		var es = $(this).val();
		listar(es);
	});

	$("#banners .lista" ).sortable({
		items: "> tr",
		opacity: 0.7,
		update:function(event,ui){

			$(".orden").show();

		}
	});

	var imagen = $('#modalagregar .cuerpo-modal').html();
	$("#modalagregar select[name=tipo]").bind("change",function(){

		//console.log(imagen);
		if($(this).val() == 1 ){
			$('#modalagregar .cuerpo-modal').html('');
			$('#modalagregar .cuerpo-modal').html(imagen)
		}
		else{
			$('#modalagregar .cuerpo-modal').html('');
			$('#modalagregar .cuerpo-modal').html(tipo_video);
		}
	});

	$("#banners .orden .btn-guardar").click(function(){
		var ids = $( "#banners .lista").sortable( "toArray",{attribute:"data-id"} );
		new Request("banner/ordenar",{
			"lista":ids.join(",")
		},function(res){
			listar(1);
			console.log("orden guardado");
		});
		$("#banners .orden").hide();

	});
	$("#banners .orden .btn-cancelar").click(function(){
		$(".orden").hide();
		$("#banners .lista").empty();
		llenar();
	})


	var options = {
		dataType: 'json',
		type: 'post',
		clearForm:true,
		success: function(res){
			console.log("HOLA");
			console.log(res.res=="ok");
			if(res.res=="ok"){
				$("#modalagregar").modal("hide");
				$(".filtro select[name=estado]").val(res.estado);
				listar(res.estado);
			}
		}
	}
    $("#modalagregar form").ajaxForm(options);

    var options = {
		dataType: 'json',
		type: 'post',
		clearForm:true,
		success: function(res){
			console.log(res.res=="ok");
			if(res.res=="ok"){
				console.log(res.res=="ok");
				$("#modaleditar").modal("hide");
				$(".filtro select[name=estado]").val(res.estado);
				listar(res.estado);
			}
		}
	}
    $("#modaleditar form").ajaxForm(options);


});

function listar(es){
	$("#banners .lista").empty();
	listahtml = Array();
	new Request("banner/listar/"+es,null,function(res){

		lista = res;

		llenar();


	});


}
function llenar(){
	$.each(lista,function(k,v){
		var it = new ItemBanner(v);
		$("#banners .lista").append(it);
	})
}



var ItemBanner = function(data){
	var img = '';
	var descripcion_tipo = '';
	var tipo = (data.bnn_tipo);
	console.log(tipo);
	if(tipo == 1)
	{
		img = path+'files/banners/'+data.bnn_foto;
		descripcion_tipo = 'Imagen';
	}
	else{
		img = path+'files/banners/'+data.bnn_foto;
		descripcion_tipo = 'Video';
	}


	var html = $('<tr width="100%" data-id="'+data.bnn_id+'">'+
					'<td><img src="' + img + '" width="50"></td>' +
					'<td>'+data.bnn_titulo+'</td>'+
					'<td>'+data.bnn_orden+'</td>'+
					'<td>'+descripcion_tipo+'</td>'+
					'<td><div class="btn-group" role="group" aria-label="...">'+
  			'<button type="button" class="btn btn-outline-primary btn-editar"><i class="fas fa-edit"></i></button>'+
			'</div></td>'+
				'</tr>');


	html.find(".btn-editar").click(function(){
		console.log(img);
			if(tipo == 1){
						$("#modaleditar .cuerpo-modal").html('');
						$("#modaleditar .cuerpo-modal").html(tipo_imagen);
						$("#modaleditar input[name=id]").val(data.bnn_id);
						$("#modaleditar input[name=nombre]").val(data.bnn_nombre);
						$("#modaleditar input[name=pre-titulo]").val(data.bnn_pretitulo);
						$("#modaleditar input[name=titulo]").val(data.bnn_titulo);
						$("#modaleditar input[name=boton]").val(data.bnn_boton);
						$("#modaleditar select[name=estado]").val(data.bnn_estado);
						$("#modaleditar select[name=posicion]").val(data.bnn_posicion);
						// $("#modaleditar .msg").attr("src","");
						// $("#modaleditar .msg").attr("src",path+"files/banners/"+data.bnn_foto);

						$("#modaleditar .preview").removeClass("pL");
						$("#modaleditar .preview").removeClass("pC");
						$("#modaleditar .preview").removeClass("pR");
						$("#modaleditar .preview").addClass("p"+data.bnn_posicion);
						$("#modaleditar .preview").css("background-image","url("+path+"files/banners/"+data.bnn_foto+")");
		}
		else{
			$("#modaleditar .cuerpo-modal").html('');
			$("#modaleditar .cuerpo-modal").append(caja_imagen);
			$("#modaleditar .cuerpo-modal").append(tipo_video);
			$("#modaleditar input[name=id]").val(data.bnn_id);
			$("#modaleditar input[name=nombre]").val(data.bnn_titulo);
			$("#modaleditar select[name=estado]").val(data.bnn_estado);
			$("#modaleditar input[name=url]").val(data.bnn_url);
			$("#modaleditar .preview").css("background-image","url("+path+"files/banners/"+data.bnn_foto+")");
		}
		$("#modaleditar").modal("show");



		$("#modaleditar a.eliminar").unbind();
		$("#modaleditar a.eliminar").click(function(){
			$("#modaleditar").modal("hide");
			$("#modaleliminar .btn-eliminar").unbind();
			$("#modaleliminar .btn-eliminar").click(function(){

				new Request("banner/eliminar/"+data.bnn_id,null,function(res){console.log(data.bnn_id);
					listar(data.bnn_estado);
					$("#modaleliminar").modal("hide");
				});
			});


		})

	});
	return html;


}
