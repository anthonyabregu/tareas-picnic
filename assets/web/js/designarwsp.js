$(document).ready(function () {

    $(".modal input[name=usuario]").val(usuario);

    listar(1);
    proyectos();
    //listarContacto();

    $(".filtros select[name=estado]").bind("change", function () {
        var est = $(this).val();
        listar(est);
    });

    

    var options = {
        dataType: 'json',
        type: 'post',
        clearForm: true,
        success: function (res) {
            if (res.res == "ok") {
                $("#modalagregar").modal("hide");
                $(".modal input[name=usuario]").val(usuario);
                proyectos();
                listar(1);
            }else{
                alert(res.res);
                $("#modalagregar").modal("hide");
                $(".modal input[name=usuario]").val(usuario);
                proyectos();
            }
        }
    }

    $("#modalagregar form").ajaxForm(options);

    var options = {
        dataType: 'json',
        type: 'post',
        clearForm: true,
        success: function (res) {
            if (res.res == "ok") {
                $("#modaleditar").modal("hide");
                listar(res.estado);
                $(".filtros select[name=estado]").val(res.estado);
            }
        }
    }
    $("#modaleditar form").ajaxForm(options);

    /*html.find(".btn-editar").click(function () {
        alert("HOLA");
        $("#modaleditar").modal("show");
    });*/

});


function proyectos(){
    new Request("proyecto/listar_proyectos/",{
        estado: 1
    },function(res){
        console.log(res);
        $.each(res,function(k,v){
            var option = '<option value="'+v.pry_id+'">'+v.pry_descripcion+'</option>'
            $("select[name=proyecto]").append(option);
            // $("#modalagregar #cat_add select[name=categoria]").append(option);
        });

         //listar();
    });
}



function listar(est) {
    console.log('log');
    $("#designar .lista").empty();

    new Request("designarwsp/listar/", {
        usw_id : usuario,
        estado: est
    }, function (res) {
        $("#designar .lista").empty();
        $.each(res, function (k, v) {
            var it = new ItemProyectos(v);
            $("#designar .lista").append(it);
        })

    });


}


var ItemProyectos = function (data) {

	var usw_id = data.usw_id;
    var id_proyecto = data.id_proyecto;
    var proyecto = data.proyecto;
    var tipoproyecto = data.tipo_proyecto;
    var estado = data.estado;

    var html = $('<tr width="100%">' +
        '<td style="vertical-align:middle;">' + id_proyecto + '</td>' +
        '<td style="vertical-align:middle;">' + proyecto + '</td>' +
        '<td style="vertical-align:middle;">' + tipoproyecto + '</td>' +
        '<td><div class="btn-group" role="group" aria-label="...">' +
        '<button type="button" class="btn btn-primary btn-editar" style="margin-right: 5px;" ><i class="fa fa-edit"></i></button>' +

        '</div></td>' +
        '</tr>');

        html.find(".btn-editar").click(function () {

        	$("#modaleditar input[name=usuario]").val(usw_id);
        	$("#modaleditar select[name=proyecto]").val(id_proyecto);
            $("#modaleditar select[name=estado]").val(estado);

            $("#modaleditar").modal("show");

            $("#modaleditar a.eliminar").unbind();
            $("#modaleditar a.eliminar").click(function () {
                $("#modaleditar").modal("hide");
                $("#modaleliminar .btn-eliminar").unbind();
                $("#modaleliminar .btn-eliminar").click(function () {

                    $("#modaleliminar").modal("show");
                    new Request("designarwsp/eliminar/" + usw_id, {
                    }, function (res) {
                        listar(1);
                        $("#modaleliminar").modal("hide");
                    });
                });

            })

            


        });

    return html;
};
