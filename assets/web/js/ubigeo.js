var ubigeo;
$(document).ready(function(){

	ubigeo = new Ubigeo();


});

var Ubigeo = function(){

	this.data = null;

	this.cargar = function(callback){
		if(ubigeo.data==null){
			new Request("ubigeo/listar",null,function(res){
				ubigeo.data = res;
				callback();
			})
		}else{
			callback();
		}

	}

	this.departamentos = function(){

		return ubigeo.data.filter(x => x.ubg_provincia === '');

	}
	this.provincias = function(dep){

		return ubigeo.data.filter(x => x.ubg_distrito === '' && x.ubg_provincia!='' && x.ubg_id.substr(0,2)===dep);

	}
	this.distritos = function(prov){
		return ubigeo.data.filter(x => x.ubg_distrito != '' && x.ubg_id.substr(0,4)===prov);		
	}

	this.llenardep = function(form){
		$(form+" select[name=departamento]").empty();
  		$(form+" select[name=departamento]").append('<option value="">Seleccione...</option>');
  		$(form+" select[name=provincia]").empty();
  		$(form+" select[name=distrito]").empty();

  		$.each(ubigeo.departamentos(),function(k,v){
  			var op = '<option value="'+v.ubg_id.substr(0,2)+'">'+v.ubg_departamento+'</option>';
  			$(form+" select[name=departamento]").append(op);

  		});
	}

	this.onseldep = function(form){

		$(form+" select[name=provincia]").empty();
		$(form+" select[name=distrito]").empty();
		$(form+" select[name=provincia]").append('<option value="">Seleccione...</option>');

		var dep = $(form+" select[name=departamento]").val();
		$.each(ubigeo.provincias(dep),function(k,v){

			var op = '<option value="'+v.ubg_id.substr(0,4)+'">'+v.ubg_provincia+'</option>';
	  		$(form+" select[name=provincia]").append(op);

		});

	}
	this.onselprov = function(form){
		$(form+" select[name=distrito]").empty();
		$(form+" select[name=distrito]").append('<option value="">Seleccione...</option>');

		var prov = $(form+" select[name=provincia]").val();
		$.each(ubigeo.distritos(prov),function(k,v){

			var op = '<option value="'+v.ubg_id+'">'+v.ubg_distrito+'</option>';
	  		$(form+" select[name=distrito]").append(op);

		});
	}
}
