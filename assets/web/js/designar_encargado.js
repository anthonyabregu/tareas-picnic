$(document).ready(function () {

    $(".modal input[name=proyecto]").val(proyecto);

    listar();
    encargados();
    //listarContacto();

       

    var options = {
        dataType: 'json',
        type: 'post',
        clearForm: true,
        success: function (res) {
            if (res.res == "ok") {
                $("#modalagregar").modal("hide");
                $(".modal input[name=proyecto]").val(proyecto);
                $("#modalagregar select[name=rol]").val("");
                encargados();
                listar();
            }else{
                alert(res.res);
                $("#modalagregar").modal("hide");
                $(".modal input[name=proyecto]").val(proyecto);
                $("#modalagregar select[name=encargado]").val("");
                $("#modalagregar select[name=rol]").val("");
                proyectos();
            }
        }
    }

    $("#modalagregar form").ajaxForm(options);

    var options = {
        dataType: 'json',
        type: 'post',
        clearForm: true,
        success: function (res) {
            if (res.res == "ok") {
                $("#modaleditar").modal("hide");
                listar(res.estado);
                $(".filtros select[name=estado]").val(res.estado);
            }
        }
    }
    $("#modaleditar form").ajaxForm(options);

    /*html.find(".btn-editar").click(function () {
        alert("HOLA");
        $("#modaleditar").modal("show");
    });*/

});


function encargados(){
    new Request("designar_encargado/listar_encargados/",{
        pry_id: proyecto
    },function(res){
        $("select[name=encargado]").html('<option value="">Seleccione...</option>');
        console.log(res);
        $.each(res,function(k,v){
            var option = '<option value="'+v.id+'">'+v.nombres+'</option>'
            $("select[name=encargado]").append(option);
            // $("#modalagregar #cat_add select[name=categoria]").append(option);
        });

         //listar();
    });
}



function listar() {
    console.log('log');
    $("#designar .lista").empty();

    new Request("designar_encargado/encargados/", {
        pry_id : proyecto
    }, function (res) {
        $("#designar .lista").empty();
        $.each(res, function (k, v) {
            var it = new ItemEncargados(v);
            $("#designar .lista").append(it);
        })

    });


}


var ItemEncargados = function (data) {

    var id = data.id;
    var nombres = data.nombres;
    var email = data.email;
    var rol = data.roles;

    var idproyecto = data.id_proyecto;

    if(rol == "1"){
        roldescripcion = "General";
    }
    if(rol == "2"){
        roldescripcion = "Contactos";   
    }
    if(rol == "3"){
        roldescripcion = "Referidos";
    }
    if(rol == "4"){
        roldescripcion = "Whatsapp";   
    }

    

    var html = $('<tr width="100%">' +
        '<td style="vertical-align:middle;">' + id + '</td>' +
        '<td style="vertical-align:middle;">' + nombres + '</td>' +
        '<td style="vertical-align:middle;">' + email + '</td>' +
        '<td style="vertical-align:middle;">' + roldescripcion + '</td>' +
        '<td><div class="btn-group" role="group" aria-label="...">' +
        '<button type="button" class="btn btn-primary btn-editar" style="margin-right: 5px;" ><i class="fa fa-edit"></i></button>' +

        '</div></td>' +
        '</tr>');

        html.find(".btn-editar").click(function () {

            /*$("#modaleditar input[name=id]").val(id);
            $("#modaleditar input[name=nombres]").val(nombre);
            $("#modaleditar input[name=apellidos]").val(apellidos);
            $("#modaleditar input[name=dni]").val(dni);
            $("#modaleditar input[name=email]").val(email);
            $("#modaleditar input[name=celular]").val(telefono);
            $("#modaleditar select[name=estado]").val(estado);*/

            $("#modaleditar").modal("show");

              //$("#modaleditar").modal("hide");
            $("#modaleditar .btn-eliminar").unbind();
            $("#modaleditar .btn-eliminar").click(function () {

                new Request("designar_encargado/eliminar/" + id +"/" + idproyecto + "/" + rol, {
                }, function (res) {
                    listar();
                    $("#modaleditar").modal("hide");
                });
            });

            


        });

    return html;
};


