var pg = 1;
var cant = 200;
var dir = "DESC";

$(document).ready(function () {

    $("#fechas").val("");

    //listar();

    $(".filtros select[name=proyecto]").bind("change", function () {

        var rango = $("input[name=daterange]").val();
        var pry_id = $(this).val();

        listar_contacto(pry_id,idUsuario,tipoUsuario,rango,pg,cant);
    })

    new Request("proyecto/listar_proyectos/",{
        estado: 1,
        usr_id: idUsuario,
        usr_tipo: tipoUsuario,
        rol: 2
    },function(res){
        console.log(res);
        $.each(res,function(k,v){
            var option = '<option value="'+v.pry_id+'">'+v.pry_descripcion+'</option>'
            $("select[name=proyecto]").append(option);
            // $("#modalagregar #cat_add select[name=categoria]").append(option);
        });

         
    });
    
    

    var options = {
        dataType: 'json',
        type: 'post',
        clearForm: true,
        success: function (res) {
            if (res.res == "ok") {
                $("#modalagregar").modal("hide");
                listar();
            }
        }
    }

    $("#modalagregar form").ajaxForm(options);

    var options = {
        dataType: 'json',
        type: 'post',
        clearForm: true,
        success: function (res) {
            if (res.res == "ok") {
                $("#modaleditar").modal("hide");
                listar();
            }
        }
    }
    $("#modaleditar form").ajaxForm(options);


    $("select[name=pagina]").bind("change",function(){
        pg = $(this).val();
        pry_id = $(".filtros select[name=proyecto]").val();
        rango = $("input[name=daterange]").val();

        listar_contacto(pry_id,idUsuario,tipoUsuario,rango,pg,cant);
    });

});



function listar() {
    console.log('log');
    $("#tabla-contacto tbody").empty();

    new Request("contacto/listar/", {
        usr_id: idUsuario,
        usr_tipo: tipoUsuario
    }, function (res) {
        console.log(res);
        $("#tabla-contacto tbody").empty();
        $.each(res, function (k, v) {
            var it = new ItemContacto(v);
            $("#tabla-contacto tbody").append(it);
        })
            

    });


}



$(".filtros input[name=daterange]").bind("change", function () {

        var pry_id = $(".filtros select[name=proyecto]").val();
        var rango = $(this).val();

        listar_contacto(pry_id,idUsuario,tipoUsuario,rango,pg,cant);
       
    });




var ItemContacto = function (data) {

    var id = data.id;
    var nombre = data.nombre;
    //var apellidos = data.cto_apellidos;
    var dni = data.dni;
    var email = data.email;
    var telefono = data.celular;
    var mensaje = data.mensaje;
    var canal = data.canal;
    var registro = data.registro;
    var idproyecto = data.idproyecto;
    var proyecto = data.proyecto;

    switch(canal){
        case "w": canal = "Web"; break;
        case "f": canal = "Facebook"; break;
    }

    var html = $('<tr width="100%">' +
        '<td style="vertical-align:middle;">' + registro + '</td>' +
        '<td style="vertical-align:middle;">' + nombre + '</td>' +
        '<td style="vertical-align:middle;">' + email + '</td>' +
        '<td style="vertical-align:middle;">' + proyecto + '</td>' +
        '<td><div class="btn-group" role="group" aria-label="...">' +
        '<button type="button" class="btn btn-outline-primary btn-editar" ><i class="fa fa-eye"></i> Detalle</button>' +
        '</div></td>' +
        '</tr>');

        html.find(".btn-editar").click(function () {



            $("#modaleditar input[name=id]").val(id);
            $("#modaleditar input[name=nombre]").val(nombre);
            //$("#modaleditar input[name=apellido]").val(apellidos);
            $("#modaleditar input[name=dni]").val(dni);
            $("#modaleditar input[name=email]").val(email);
            $("#modaleditar input[name=celular]").val(telefono);
            $("#modaleditar textarea[name=mensaje]").val(mensaje);
            $("#modaleditar input[name=canal]").val(canal);
            $("#modaleditar input[name=proyecto]").val(proyecto);

            $("#modaleditar").modal("show");

            $("#modaleditar a.eliminar").unbind();
            $("#modaleditar a.eliminar").click(function () {
                $("#modaleditar").modal("hide");
                $("#modaleliminar .btn-eliminar").unbind();
                $("#modaleliminar .btn-eliminar").click(function () {

                    new Request("contacto/eliminar/" + data.cto_id, {
                    }, function (res) {
                        listar();
                        $("#modaleliminar").modal("hide");
                    });
                });

            })


        });

    return html;
};


function listar_contacto(pry_id,idUsuario,tipoUsuario,rango,pg,cant){

    $('#tabla-contacto tbody').html("");

        new Request("contacto/listar_contactos/",{
                pry_id: pry_id,
                usr_id: idUsuario,
                usr_tipo: tipoUsuario,
                rango: rango,
                pg : pg,
                cant: cant
            },function(res){
                //console.log(res);
                $("#tabla-contacto tbody").empty();
                
                if(res.res == "ok"){
                    var ini = (pg-1)*cant + 1;
                    var fin = ini+cant - 1;

                    var tot = res.total;
                    if(fin>tot) fin=tot;
                    $("#contacto .cant").html("Registros "+ini+" al "+fin+" de "+tot);

                    $.each(res.lista,function(k,v){
                        var it = new ItemContacto(v);
                        $("#tabla-contacto tbody").append(it);
                    });

                    var cantpg = Math.ceil(tot/cant);

                    $("select[name=pagina]").empty();
                    for(var i=0;i<cantpg;i++){
                        $("select[name=pagina]").append('<option value="'+(i+1)+'">'+(i+1)+'</option>');
                    }

                    $("select[name=pagina]").val(pg);

                }else{
                    $("#contacto .cant").html("Sin Registros");
                    alert("No se encontraron registros en esta fecha");
                }                
        })
} 